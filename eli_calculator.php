<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Electricity Rates Calculator</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
</head>
<body>
  <div class="container mt-5">
    <h1 class="mb-4">Electricity Rates Calculator</h1>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
      <div class="form-group">
        <label for="voltage">Voltage (V)</label>
        <input type="number" step="0.01" class="form-control" id="voltage" name="voltage" required>
      </div>
      <div class="form-group">
        <label for="current">Current (A)</label>
        <input type="number" step="0.01" class="form-control" id="current" name="current" required>
      </div>
      <div class="form-group">
        <label for="rate">Rate (cents/kWh)</label>
        <input type="number" step="0.01" class="form-control" id="rate" name="rate" required>
      </div>
      <button type="submit" class="btn btn-primary">Calculate</button>
    </form>
    <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          // Get input values
          $voltage = $_POST['voltage'];
          $current = $_POST['current'];
          $rate = $_POST['rates'];
            
            function calculateElectricityRates($voltage, $current, $rate) {
              // Calculate power in watts
              $power = $voltage * $current;

              // Calculate energy in kilowatt-hours
              $energy = ($power / 1000) * 1; // assuming 1 hour of usage

              // Calculate total charge per hour
              $chargePerHour = $energy * ($rate / 100);

              // Calculate total charge per day (assuming 24 hours of usage)
              $chargePerDay = $chargePerHour * 24;

              // Return an array containing all the calculated values
              return [
                'power' => $power,
                'energy' => $energy,
                'chargePerHour' => $chargePerHour,
                'chargePerDay' => $chargePerDay
              ];
            }

          // Call the calculation function
          $rates = calculateElectricityRates($voltage, $current, $rate);

            
          // Output the calculated values
          echo '<div class="mt-4"><h3>Results:</h3>';
          echo '<p>POWER: ' . $rates['energy'] . ' kWh</p>';
          echo '<p>RATE: ' . $rate/100 . 'RM</p>';
        }
    ?>
      
      
</div>
</body>
</html>